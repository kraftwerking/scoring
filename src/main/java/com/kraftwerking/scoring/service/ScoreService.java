package com.kraftwerking.scoring.service;

import java.util.ArrayList;

public interface ScoreService {
    int getScoreForList(ArrayList<String> namesList);

    int getScoreForName(String name, int pos);

    int letterToAlphabetPos(char letter);
}
