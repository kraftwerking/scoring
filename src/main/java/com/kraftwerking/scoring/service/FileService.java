package com.kraftwerking.scoring.service;

import java.util.ArrayList;

public interface FileService {
    void getScoreForFile(String filePath);

    ArrayList<String> parseNamesFile(String filePath);
}
