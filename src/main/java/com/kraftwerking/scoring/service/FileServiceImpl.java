package com.kraftwerking.scoring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

@Service
public class FileServiceImpl implements FileService {
    @Value("${message.begin.scoring}")
    private String msgBeginScoring;

    @Value("${message.end.scoring}")
    private String msgEndScoring;

    @Autowired
    private ScoreServiceImpl scoreServiceImpl;

    private static final Logger LOG = LoggerFactory.getLogger(FileServiceImpl.class);
    private static final String COMMA_DELIMITER = ",";

    @Override
    public void getScoreForFile(String filePath) {
        LOG.info(msgBeginScoring + filePath);
        ArrayList<String> namesList = parseNamesFile(filePath);
        int score = scoreServiceImpl.getScoreForList(namesList);
        LOG.info(msgEndScoring + score);
    }

    @Override
    public ArrayList<String> parseNamesFile(String filePath) {
        ArrayList<String> namesList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                for (String s : values) {
                    namesList.add(s.replace("\"", ""));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(namesList);
        return namesList;
    }
}
