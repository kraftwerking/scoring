package com.kraftwerking.scoring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ScoreServiceImpl implements ScoreService {

    private static final Logger LOG = LoggerFactory.getLogger(ScoreServiceImpl.class);

    @Override
    public int getScoreForList(ArrayList<String> namesList) {
        List<Integer> scoreList = IntStream.range(0, namesList.size())
                .mapToObj(i -> getScoreForName(namesList.get(i), i + 1))
                .collect(Collectors.toList());

        return scoreList.stream()
                .reduce(0, (a, b) -> a + b);
    }

    @Override
    public int getScoreForName(String name, int pos) {
        LOG.debug(name);
        int tmpScore = 0;

        for (char c : name.toCharArray()) {
            LOG.debug(c + " " + letterToAlphabetPos(c));
            tmpScore = tmpScore + letterToAlphabetPos(c);
        }

        LOG.debug("tmpSc " + tmpScore + " * pos " + pos + " = " + tmpScore * pos);
        return tmpScore * pos;
    }

    @Override
    public int letterToAlphabetPos(char letter) {
        return Character.toUpperCase(letter) - 64;
    }
}
