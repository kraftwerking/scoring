package com.kraftwerking.scoring;

import com.kraftwerking.scoring.service.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@SpringBootApplication
public class ScoringApplication implements CommandLineRunner {
    @Autowired
    private ConfigurableApplicationContext context;

    @Autowired
    private FileServiceImpl fileServiceImpl;

    public static void main(String[] args) {
        SpringApplication.run(ScoringApplication.class, args);
    }

    @Override
    public void run(String... args) {
        String filePath = (args.length > 0) ? args[0] : null;
        if (filePath != null) {
            fileServiceImpl.getScoreForFile(filePath);
        }
    }
}
