package com.kraftwerking.scoring;

import com.kraftwerking.scoring.service.FileServiceImpl;
import com.kraftwerking.scoring.service.ScoreServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ScoringApplicationTests {
    @Value("${file.path}")
    private String filePath;

    @Value("${file.path2}")
    private String filePath2;

    @Autowired
    private FileServiceImpl fileServiceImpl;

    @Autowired
    private ScoreServiceImpl scoreServiceImpl;

    @Test
    void testParseNamesFile() {
        ArrayList<String> namesList = fileServiceImpl.parseNamesFile(filePath);
        assertEquals(namesList.size(), 9);
        assertEquals(namesList.get(0), "BARBARA");
    }

    @Test
    void testLetterToAlphabetPos() {
        int pos = scoreServiceImpl.letterToAlphabetPos('c');
        assertEquals(pos, 3);
    }

    @Test
    void testGetScoreForName() {
        int scoreForName = scoreServiceImpl.getScoreForName("LINDA", 4);
        assertEquals(scoreForName, 160);
    }

    @Test
    void testGetScoreForList() {
        ArrayList<String> namesList = fileServiceImpl.parseNamesFile(filePath);
        int score = scoreServiceImpl.getScoreForList(namesList);
        assertEquals(score, 3194);
        namesList = fileServiceImpl.parseNamesFile(filePath2);
        score = scoreServiceImpl.getScoreForList(namesList);
        assertEquals(score, 871198282);
    }
}
