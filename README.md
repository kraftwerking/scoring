# SCORING APPLICATION
This is my implementation of the scoring assignment. Please refer to the [Interview Coding Assignment document](file:////Interview Coding Assignment.docx) for project requirements.  The idea for my solution for this assignment is a simple Java Gradle solution that satisfies the base requirements of the assignment with a minimum of third party dependencies 

- with classes organized around fine grained business functionalities and the single responsibility principle as much as possible
- using interfaces for services to promote multiple implementation of base behavior in implementing classes, which will help when more complex scoring algorithms are added in the future or when service code needs to be modified to work withe Spring MVC REST Controllers
- use of Spring DI to minimize code, loose coupling of dependecies

### REQUIREMENTS
Java >= 1.8, Gradle > 6.3

### BUILDING AND TESTING

    ./gradlew clean build
    ./gradlew test
    ./gradlew bootRun --args PATH_TO_FILE

This project can also be imported into Intellij IDEA and run with gradle plugin support.
